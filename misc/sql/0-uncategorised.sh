#!/bin/sh
# seek for packages maintained by a Blends team which is not mentioned in any task

if [ $# -lt 1 ] ; then
    echo "Usage: $0 <blend>"
    exit 1
fi

case "$1" in
    debian-science)
        team="'debian-science-maintainers@lists.alioth.debian.org','pkg-scicomp-devel@lists.alioth.debian.org','pkg-electronics-devel@lists.alioth.debian.org'"
        ignore="'arachne-pnr','arduino-builder','armci-mpi',
                'cbp2make','cctz','coinor-cgl','coinor-osi','coinutils','covered','cpuinfo',
                'dh-fortran-mod','dijitso','docopt.cpp',
                'dune-common', 'dune-geometry', -- Dependencies of dune-grid
                'farmhash',
                'ghdl','giella-core',
                'gmp',                          -- very basic, used in lots of other packages as dependency
                'gnucap-python','graywolf',
                'heaptrack','highwayhash','hmat-oss',
                'imview-doc', 'instant','itango','iverilog',
                'libcerf','libgdsii','libgpuarray','libmawk','libosmocore','libserialport',
                'lua-torch-cwrap','lua-torch-dok','lua-torch-paths','lua-torch-sundown',
                'kwwidgets',
                'mpi-defaults','node-pinkyswear','nsync',
                'openbsc','openems','openggsn','osmo-bts','osmo-trx','libosmo-abis','libosmo-netif','libosmo-sccp','libsmpp34',
                'pcb','pcb-rnd',
                'polyml','python-backports.tempfile','python-backports.weakref','python-cymem','python-jsondiff',
                'python3-line-profiler','python-memory-profiler','python-nose-random','python-pyorick',
                'ros-actionlib','ros-angles','ros-bloom','ros-bond-core','ros-catkin','ros-catkin-pkg','ros-class-loader',
                'ros-cmake-modules','ros-common-msgs','ros-dynamic-reconfigure','ros-eigen-stl-containers',
                'ros-gencpp','ros-genlisp','ros-genmsg','ros-genpy','ros-geometric-shapes','ros-geometry',
                'ros-geometry-experimental','ros-image-common','ros-interactive-markers','ros-laser-geometry',
                'ros-message-generation','ros-message-runtime','ros-navigation-msgs','ros-nodelet-core',
                'ros-pcl-conversions','ros-pcl-msgs','ros-pluginlib','ros-python-qt-binding','ros-random-numbers',
                'ros-resource-retriever','ros-robot-model','ros-ros-comm','ros-ros-comm-msgs','ros-rosconsole-bridge',
                'ros-ros','ros-roscpp-core','ros-rosdep','ros-rosdistro','ros-rosinstall','ros-rosinstall-generator',
                'ros-roslisp','ros-rospack','ros-rospkg','ros-rviz','ros-std-msgs','ros-vcstools','ros-vision-opencv',
                'ros-wstool',
                'qelectrotech','qflow',
                'tbb','togl',
                'vmtk',		-- is mentioned in Debian Med; there is no really good fitting task in Debian Science
                'xmds-doc',
                'wxastrocapture',
                'yp-svipc',
                'yorick-av',
                'yorick-curses',
                'yorick-gl',
                'yorick-gy',
                'yorick-hdf5',
                'yorick-imutil',
                'yorick-ml4',
                'yorick-mpeg',
                'yorick-mira',	-- belongs to Debian Astro and should not be listed in Debian Science
                'yorick-optimpack',
                'yorick-soy',
                'yorick-ygsl',
                'yorick-yeti',
                'yorick-ynfft',
                'yorick-yutils',
                'yorick-z'"
        ;;
    debian-med)
        team="'debian-med-packaging@lists.alioth.debian.org'"
        # these source packages are simple preconditions which should not appear in any task
        ignore="'biomaj3-core', 'biomaj3-download', 'biomaj3-process', 'biomaj3-user', 'biomaj3-zipkin', 'bitops',
                'catools','charls','circos-tools','colt','conda-package-handling','ctn-doc',
                'datatables.js','dichromat',
                'enlighten','f2j','hopscotch-map',
                'g2','golang-github-dataence-porter2','gtable',
                'igraph','iitii',
                'jai-core','jai-imageio-core','jam-lib','jheatchart','jlapack',
                'labeling','libatomicbitvector','libcereal','libcolt-free-java','libdeflate','libdistlib-java','libgtextutils',
                'libgzstream','libhac-java','libhat-trie','libhpptools','libips4o','libjbzip2-java','libjung-free-java',
                'libkmlframework-java','liblemon','libmmap-allocator','liboptions-java','libquazip','librandom123',
                'librg-exception-perl','libsis-base-java','libsis-jhdf5-java','libtecla','libundead','libzeep','libzstd',
                'metaphlan2-data','metastudent-data','metastudent-data-2',
                'mgltools-bhtree','mgltools-cadd','mgltools-cmolkit','mgltools-dejavu','mgltools-geomutils','mgltools-gle','mgltools-mglutil','mgltools-molkit','mgltools-networkeditor',
                'mgltools-opengltk','mgltools-pmv','mgltools-pyautodock','mgltools-pybabel','mgltools-pyglf','mgltools-scenario2','mgltools-sff','mgltools-support','mgltools-symserv',
                'mgltools-utpackages','mgltools-viewerframework','mgltools-vision','mgltools-visionlibraries','mgltools-volume','mgltools-webservices',
                'mpj','mtj','mummy','mypy',
                'netlib-java',
                'parafly', 'paryfor', 'patsy', 'permute','pixelmed-codec','pp-popularity-contest','pvrg-jpeg','pylibtiff','pyode','pyrle','pyqi',
                'python-avro','python-qcli','python-duckpy',
                'pythonqt','python-burrito','python-bz2file','python-colormap','python-colormath','python-depinfo','python-dictobj','python-easydev','python-hdmedians',
                'python-lzstring','python-matplotlib-venn','python-pipdeptree',
                'python-rdflib-jsonld','python-schema-salad','python-spectra','python-sqlsoup','python-streamz','python3-typed-ast','python-xopen',
                'qsopt-ex',
                'readerwriterqueue','routine-update',
                'simde','smart-open','snappy1.0.3-java','socket++','sorted-nearest','spdlog','sphinxcontrib-autoprogram',
                'tao-config','tao-json','yaggo'
               "
        ;;
    debian-gis)
        team="'pkg-grass-devel@lists.alioth.debian.org','pkg-osm-maint@lists.alioth.debian.org'"
        ignore="'ruby-narray-miss'"
        ;;
    debian-astro)
        team="'debian-astro-maintainers@lists.alioth.debian.org'"
        ignore="''"
        ;;
    debichem)
        team="'debichem-devel@lists.alioth.debian.org'"
        ignore="'cclib-data',
                'garlic-doc',
                'r-other-iwrlar'"
        ;;
    *)
        echo "Unsupported Blend $1"
        exit 1
        ;;
esac

if [ ! $(which psql) ] ; then
    cat <<EOT
No PostgreSQL client providing /usr/bin/psql installed.
On a Debian system please
    sudo apt-get install postgresql-client-common
EOT
    exit 1
fi

SERVICE="service=udd"
#if there is a local UDD clone just use this
if psql $PORT -l 2>/dev/null | grep -qw udd ; then
    SERVICE=udd
fi

# Check UDD connection
if ! psql $PORT $SERVICE -c "" 2>/dev/null ; then
    echo "No local UDD found, use public mirror."
    PORT="--port=5432"
    export PGPASSWORD="public-udd-mirror"
    SERVICE="--host=public-udd-mirror.xvm.mit.edu --username=public-udd-mirror udd"
fi

if which psql-public-udd >/dev/null ; then
   RUNPSQL=psql-public-udd
else
   RUNPSQL="psql $SERVICE"
fi

$RUNPSQL >$1-uncategorised.out <<EOT
SELECT t.source, s.bin, u.changed_by FROM (
  SELECT source FROM (
    SELECT DISTINCT source, CASE WHEN b.package IS NULL THEN 0 ELSE 1 END AS is_in_task FROM packages p
      LEFT JOIN blends_dependencies b ON p.package = b.package
      WHERE maintainer_email IN ($team)
        AND release = 'sid'                                 -- restrict to packages in unstable for the moment
        AND (b.dependency IS NULL OR b.dependency != 'i')
        AND (b.blend      IS NULL OR b.blend = '$1')
        AND source != '$1'
        AND source NOT IN ($ignore)
    ) tmp
    GROUP BY source
    HAVING MAX(is_in_task) = 0
  ) t
  LEFT JOIN (SELECT source, bin, row_number() OVER (PARTITION BY source ORDER BY version DESC) AS sver FROM sources WHERE release = 'sid') s ON t.source = s.source
  LEFT JOIN (SELECT source, changed_by, row_number() OVER (PARTITION BY source ORDER BY date DESC) AS udate FROM upload_history) u ON u.source = s.source
  WHERE s.sver = 1 and u.udate = 1
  ORDER BY source
  ;
EOT


exit 0

