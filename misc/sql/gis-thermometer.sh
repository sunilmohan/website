#!/bin/sh
# This script was used to do tests to implement webtools/thermometer.py

if [ $# -lt 1 ] ; then
    echo "$0: Argument for Blend is missing"
    exit
fi

# Note: The query below does not regard packages not available under a source
# name in stable or testing that is different from the name in unstable

SERVICE="service=udd"
#if there is a local UDD clone just use this
if psql -l 2>/dev/null | grep -qw udd ; then
    SERVICE=udd
fi

case "$1" in
    debian-science)
        team="'debian-science-maintainers@alioth-lists.debian.net','debian-science-maintainers@lists.alioth.debian.org','pkg-scicomp-devel@lists.alioth.debian.org'"
        ;;
    debian-med)
        team="'debian-med-packaging@alioth-lists.debian.net','debian-med-packaging@lists.alioth.debian.org'"
        ;;
    debian-gis)
        team="'pkg-grass-devel@alioth-lists.debian.net','pkg-grass-devel@lists.alioth.debian.org','pkg-osm-maint@lists.alioth.debian.org'"
        ignore="''"
        ;;
    *)
        echo "Unsupported Blend $1"
#        exit 1
        ;;
esac


psql $SERVICE >$1.out <<EOT
SELECT b.source,
       stable.version AS stable,
       testing.version AS testing,
       unstable.version AS unstable,
       stable_bpo.version AS "stable-bpo",
       experimental.version AS experimental,
       unreleased.version AS "UNRELEASED",
       precise.version AS precise,
       quantal.version AS quantal,
       raring.version AS raring,
       d.upstream_version AS upstream,
       -- map values from former dehs to upstream table values
       CASE WHEN d.status IS NULL OR
                 d.status = '' OR
                 d.status = 'error'                                 THEN 'none'
            WHEN d.status = 'Newer version available'               THEN 'outdated'
            WHEN d.status = 'up to date'                            THEN 'upToDate'
            WHEN d.status = 'Debian version newer than remote site' THEN 'newer-in-debian'
            ELSE 'unknown' -- should not occure!
       END AS upstreamstatus,
       homepage,
       wnpp,
       tasks.tasks,
       CASE WHEN stable.version >= unstable.version OR replace(stable_bpo.version, '~bpo.*', '') >= unstable.version THEN 'upToDate'
            WHEN stable.version <  unstable.version OR replace(stable_bpo.version, '~bpo.*', '') <  unstable.version THEN 'debianOutOfDate'
            WHEN stable.version IS NOT NULL AND unstable.version IS NULL THEN 'obsolete'
            WHEN stable.version IS NULL AND testing.version IS NULL AND unstable.version IS NULL AND new.version IS NULL THEN 'unpackaged'
            WHEN new.version IS NULL AND (experimental.version IS NOT NULL OR unreleased.version IS NOT NULL) THEN 'workInProgress'
            WHEN new.version IS NOT NULL THEN 'new'
            ELSE 'unknown' END AS debianstatus
 FROM (
  SELECT DISTINCT p.source, '' AS wnpp FROM packages p
  JOIN blends_dependencies bd ON bd.package = p.package
  JOIN releases r ON p.release = r.release
  WHERE bd.blend = '$1' AND
        (r.sort >= (SELECT sort FROM releases WHERE role = 'stable') OR r.sort = 0) -- forget older releases than stable but allow experimental
  UNION
  SELECT DISTINCT u.source, '' AS wnpp FROM ubuntu_packages u
  JOIN blends_dependencies bd ON bd.package = u.package
  WHERE bd.blend = '$1' AND bd.distribution = 'ubuntu'
  UNION
  SELECT DISTINCT pr.source, CASE WHEN wnpp!=0 THEN CAST(pr.wnpp AS text) ELSE '' END AS wnpp FROM blends_prospectivepackages pr
  JOIN blends_dependencies bd ON bd.package = pr.package
  WHERE bd.blend = '$1' AND bd.distribution = 'prospective'
 ) b
 LEFT OUTER JOIN ( SELECT source, homepage FROM (
  SELECT source, homepage, row_number() OVER (PARTITION BY source ORDER BY version DESC) FROM (
   SELECT DISTINCT p.source, p.homepage, p.version FROM packages p
     JOIN blends_dependencies bd ON bd.package = p.package
     JOIN releases r ON p.release = r.release
     WHERE bd.blend = '$1' AND
        (r.sort >= (SELECT sort FROM releases WHERE role = 'stable') OR r.sort = 0) -- forget older releases than stable but allow experimental
   UNION
   SELECT DISTINCT u.source, u.homepage, u.version FROM ubuntu_packages u
     JOIN blends_dependencies bd ON bd.package = u.package
     WHERE bd.blend = '$1' AND bd.distribution = 'ubuntu'
     UNION
   SELECT DISTINCT pr.source, pr.homepage,  pr.chlog_version as version FROM blends_prospectivepackages pr
     JOIN blends_dependencies bd ON bd.package = pr.package
     WHERE bd.blend = '$1' AND bd.distribution = 'prospective'
   ) hpversion
  GROUP BY source, homepage, version
  ) tmp
  WHERE row_number = 1
 ) homepage ON b.source = homepage.source
 LEFT OUTER JOIN (
  SELECT DISTINCT p.source, strip_binary_upload(MAX(s.version)) AS version
    FROM packages p
    JOIN releases r ON p.release = r.release
    JOIN blends_dependencies b ON b.package = p.package
    -- make sure we get the right source version that does not necessarily match binary version
    JOIN sources s ON p.source = s.source AND p.release = s.release
    WHERE b.blend = '$1' AND r.role = 'unstable' AND p.distribution = 'debian'
    GROUP BY p.source
    ORDER BY p.source
  ) unstable ON b.source = unstable.source
 LEFT OUTER JOIN (
  SELECT DISTINCT p.source, strip_binary_upload(MAX(s.version)) AS version
    FROM packages p
    JOIN releases r ON p.release = r.release
    JOIN blends_dependencies b ON b.package = p.package
    -- make sure we get the right source version that does not necessarily match binary version
    JOIN sources s ON p.source = s.source AND p.release = s.release
    WHERE b.blend = '$1' AND r.role = 'testing' AND p.distribution = 'debian'
    GROUP BY p.source
    ORDER BY p.source
  ) testing ON b.source = testing.source
 LEFT OUTER JOIN (
  SELECT DISTINCT p.source, strip_binary_upload(MAX(s.version)) AS version
    FROM packages p
    JOIN releases r ON p.release = r.release
    JOIN blends_dependencies b ON b.package = p.package
    -- make sure we get the right source version that does not necessarily match binary version
    JOIN sources s ON p.source = s.source AND p.release = s.release
    WHERE b.blend = '$1' AND r.role = 'stable' AND p.distribution = 'debian'
    GROUP BY p.source
    ORDER BY p.source
  ) stable ON b.source = stable.source
 LEFT OUTER JOIN (
  SELECT DISTINCT p.source, strip_binary_upload(MAX(s.version)) AS version
    FROM packages p
    JOIN releases r ON p.release = r.release
    JOIN blends_dependencies b ON b.package = p.package
    -- make sure we get the right source version that does not necessarily match binary version
    JOIN sources s ON p.source = s.source AND p.release = s.release
    WHERE b.blend = '$1' AND r.role = 'experimental' AND p.distribution = 'debian'
    GROUP BY p.source
    ORDER BY p.source
  ) experimental ON b.source = experimental.source
 LEFT OUTER JOIN (
  SELECT DISTINCT p.source, strip_binary_upload(MAX(s.version)) AS version
    FROM packages p
    JOIN releases r ON p.release = r.release || '-backports'
    JOIN blends_dependencies b ON b.package = p.package
    -- make sure we get the right source version that does not necessarily match binary version
    JOIN sources s ON p.source = s.source AND p.release = s.release
    WHERE b.blend = '$1' AND p.release = (SELECT release FROM releases WHERE role='stable') || '-backports' AND p.distribution = 'debian'
    GROUP BY p.source
    ORDER BY p.source
  ) stable_bpo ON b.source = stable_bpo.source
     LEFT OUTER JOIN (
      SELECT DISTINCT np.source, strip_binary_upload(MAX(np.version)) AS version
        FROM new_packages np
        JOIN blends_dependencies b ON b.package = np.package
        -- make sure we get the right source version that does not necessarily match binary version
        JOIN new_sources ns ON np.source = ns.source
        WHERE b.blend = '$1' AND b.distribution = 'new'
        GROUP BY np.source
        ORDER BY np.source
     ) new ON b.source = new.source
 LEFT OUTER JOIN ( -- an 'UNRELEASED' version can be due to not yet finished work in VCS or not yet uploaded at all
  SELECT DISTINCT source, version FROM (
   SELECT DISTINCT p.source, strip_binary_upload(MAX(v.version)) AS version
    FROM packages p
    JOIN blends_dependencies b ON b.package = p.package
    -- make sure we get the right source version that does not necessarily match binary version
    JOIN sources s ON p.source = s.source AND p.release = s.release
    JOIN vcs v ON s.source = v.source
    WHERE b.blend = '$1' AND v.distribution = 'UNRELEASED'
    GROUP BY p.source
   UNION
   SELECT DISTINCT pr.source, strip_binary_upload(chlog_version) AS version
    FROM blends_dependencies b
    JOIN blends_prospectivepackages pr ON b.package = pr.package
    WHERE b.blend = '$1'
   ) tmp
  ) unreleased ON b.source = unreleased.source
 LEFT OUTER JOIN (
  SELECT DISTINCT u.source, strip_binary_upload(MAX(s.version)) AS version
    FROM ubuntu_packages u
    JOIN blends_dependencies b ON b.package = u.package
    JOIN ubuntu_sources s ON u.source = s.source AND u.release = s.release
    WHERE b.blend = '$1' AND u.release = 'precise'
    GROUP BY u.source
    ORDER BY u.source
  ) precise ON b.source = precise.source
 LEFT OUTER JOIN (
  SELECT DISTINCT u.source, strip_binary_upload(MAX(s.version)) AS version
    FROM ubuntu_packages u
    JOIN blends_dependencies b ON b.package = u.package
    JOIN ubuntu_sources s ON u.source = s.source AND u.release = s.release
    WHERE b.blend = '$1' AND u.release = 'quantal'
    GROUP BY u.source
    ORDER BY u.source
  ) quantal ON b.source = quantal.source
 LEFT OUTER JOIN (
  SELECT DISTINCT u.source, strip_binary_upload(MAX(s.version)) AS version
    FROM ubuntu_packages u
    JOIN blends_dependencies b ON b.package = u.package
    JOIN ubuntu_sources s ON u.source = s.source AND u.release = s.release
    WHERE b.blend = '$1' AND u.release = 'raring'
    GROUP BY u.source
    ORDER BY u.source
  ) raring ON b.source = raring.source
 LEFT OUTER JOIN (
  SELECT source, array_agg(task) AS tasks FROM (
   SELECT DISTINCT p.source, b.task
    FROM packages p
    JOIN releases r ON p.release = r.release
    JOIN blends_dependencies b ON b.package = p.package
    -- make sure we get the right source version that does not necessarily match binary version
    JOIN sources s ON p.source = s.source AND p.release = s.release
    WHERE b.blend = '$1'
   ) tmp
   GROUP BY source
  ) tasks ON b.source = tasks.source
  LEFT OUTER JOIN (SELECT source, upstream_version, status FROM upstream WHERE release = 'sid') d ON b.source = d.source
  ORDER BY b.source
;

EOT
