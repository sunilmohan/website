#!/usr/bin/python3

"""
This script extracts numbers of dependencies of different metapackages of a Blend
"""



from sys import stderr, exit, argv
from os import listdir, path
from fnmatch import fnmatch
import re
from subprocess import Popen, PIPE
from debian import deb822
#from mx.DateTime import *
from datetime import datetime

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
import json

debug=0

# Some tasks contain only a few dependencies which are just spoiling the 3D view
# Make sure we are drawing a certain selection and all those tasks that might
# have more dependencies than these
DRAWTASKS = { 'med': ['bio', 'bio-dev', 'epi', 'practice', 'psychology'],
              'science': ['chemistry'], # ['astronomy'],   # 'dataacquisition', 'distributedcomputing', 'electronics', 'engineering'], 
              'debichem': [ 'abinitio', 'cheminformatics', 'modelling', 'molmech', 'polymer', 'semiempirical', 'view-edit-2d', 'visualisation' ],
            }
SUPPRESSTASKS = { 'med': [ 'cloud', 'covid-19', 'bio-ngs', 'bio-phylogeny' ],
                  'science' : [],
                  'debichem' : [],
                }

changelog_entry_start_re = re.compile('^[^\s]+ \((\d.+)\) unstable; urgency=')
changelog_entry_UNRELEASED_re = re.compile('^[^\s]+ \((\d.+)\) UNRELEASED; urgency=')
changelog_entry_end_re   = re.compile('^ -- .* <.*@.*>  (.*)$')

def TasksGetDrawList(tasks, checklist, avoidlist):
    try:
        task_last = tasks[sorted(tasks.keys())[-1]]
    except IndexError as err:
        print(err, file=stderr)
        print("tasks.keys() =", list(tasks.keys()), file=stderr)
        exit()
    # evaluate minimum number of dependencies to draw
    min2draw = 100
    drawlist = []
    for t in checklist:
        try:
            print(t, task_last.nrecommended[t])
        except KeyError as err:
            continue
        if task_last.nrecommended[t] < min2draw:
            min2draw = task_last.nrecommended[t]
    for r in list(task_last.recommends.keys()):
        if task_last.nrecommended[r] >= min2draw:
            if r not in avoidlist:
                drawlist.append(r)

    drawlist.sort()
    return drawlist


class taskscontent:
    def __init__(self, blend=None, version=None, date=None, htask=None, hrecommended=None):
        self.blend      = blend
        self.version	= version
        self.date	= date
        #print(date)
        try:
            self.mxdate     =  datetime.strptime(str(date), '%Y-%m-%d')
        except ValueError:
            try:
                self.mxdate     =  datetime.strptime(str(date), '%Y-%m-%d%z')
            except ValueError:
                self.mxdate     =  datetime.strptime(str(date), '%Y-%m-%d %H:%M:%S%z')
#        datetime.date.fromtimestamp(date) # DateFrom(date)
        self.year	= self.mxdate.year
        self.month	= self.mxdate.month
        self.day	= self.mxdate.day
        self.datekey    = "%04i%02i%02i" % (self.year, self.month, self.day)
        self.recommends	= {}
        self.nrecommended = {}
        if htask != None and hrecommended != None:
            self.nrecommended[htask] = int(hrecommended)
            self.recommends[htask] = []
        self.ctrlfile	= None

    def add_recommends(self, package, recommends):
        p = re.sub(self.blend+'-', '', package)

        if p not in self.recommends:
            self.recommends[p] = []
        for r in recommends:
            # since we also need to investigate "Depends" for older metapackages we also
            # need to exclude the extra control packages
            if r.startswith(self.blend+'-common') or r.startswith(self.blend+'-config') or r.startswith(self.blend+'-tasks'):
                 continue
            self.recommends[p].append(r)
        self.nrecommended[p] = len(self.recommends[p])

    def __str__(self):
        s="Version: %s; Date: %s-%02i-%02i:" % (self.version, self.year, int(self.month), int(self.day))
        for r in sorted(self.recommends.keys()):
            if r not in self.nrecommended:
                self.nrecommended[r] = len(self.recommends[r])
            s += '\n  ' + r + ': ' + str(self.nrecommended[r])
        return s


def main():
    if len(argv) < 3:
        print("Usage: %s <path_to_blends_git_repository> <blend>" % argv[0], file=stderr)
        exit()
    if argv[2] not in list(DRAWTASKS.keys()):
        print("This script is only prepared for %s" % str(VALIDBLENDS), file=stderr)
        exit()
    BLEND = argv[2]
    root = argv[1]+'/dependency_data'
    u_dirs = sorted(listdir(root))

    tasks = {}
    tasks_found = []

    # Read some data about first med-* packages which never made it into any Vcs
    if BLEND == 'med':
        fp=open('med_historical_data.json')
        mjs=fp.read()
        medhist = json.loads(mjs)
        fp.close()
        for mver in medhist:
            if 'task' not in mver:
                continue
            if mver['task'] == 'bio-contrib':
                # we are not interested in non-free dependencies
                continue
            if mver['task'] == 'dent':
                mver['task'] = 'dental'
            if not mver['task'] in tasks_found:
                tasks_found.append(mver['task'])
            task = taskscontent(BLEND, mver['version'], mver['date'], mver['task'], mver['recommends'])
            if task.datekey in tasks:
                # try to match several single metapackages to what we know today as multibinary
                mtask = tasks[task.datekey]
                if mtask.version != task.version:
                    mtask.version = mtask.version + '+' + task.version
                if mver['task'] in mtask.recommends:
                    print("Duplicated task at same date.", file=stderr)
                    continue
                mtask.recommends[mver['task']] = []
                mtask.nrecommended[mver['task']] = int(mver['recommends'])
            else:
                tasks[task.datekey] = task

    dversions={}
    for ujson in u_dirs:
        if not ujson.endswith('.json'):
            print("Warning: File %s in dir %s ignored" % (ujson, root), file=stderr)
            continue
        u = re.sub('^.+_(\d[.\d]*)\.json', '\\1', ujson)
        if len(u.split('.')) > 2:
            # print >>stderr, "Minor releases usually add no new Dependencies - version %s ignored." % u
            continue
        dversions[u] = {'jsondata': ujson,
                       }
    changelog = argv[1] + '/debian/changelog'
    if not path.exists(changelog):
        print("No such changelog %s" % changelog, file=stderr)
        exit(1)

    f = open(changelog, 'r')
#    for stanza in deb822.Sources.iter_paragraphs(f):
#        print(stanza.keys()) # does not provide sensible keys ...

    ignore=False
    for line in f.readlines():
        match = changelog_entry_start_re.match(line)
        if match:
            chversion = match.groups()[0]
            ignore=False
        else:
            match = changelog_entry_UNRELEASED_re.match(line)
            if match:
                ignore=True
                continue
        match = changelog_entry_end_re.match(line)
        if match:
            if ignore:
                ignore=False
                continue
            #chdate = datetime.fromisoformat(match.groups()[0]) # , '%Y-%m-%d') # DateFrom(match.groups()[0])
            chdate = datetime.strptime(match.groups()[0], "%a, %d %b %Y %H:%M:%S %z")
            if chversion in dversions:
                task = taskscontent(BLEND, chversion, chdate)
                tasks[task.datekey] = task
                dversions[chversion]['datekey'] = task.datekey
                jfp=open(root+'/'+dversions[chversion]['jsondata'])
                depdata=json.loads(jfp.read())
                jfp.close()
                # print(chversion, task.datekey, depdata)
                dversions[chversion]['depdata'] = depdata

#    print(dversions)

                for package in depdata:
                    if package != BLEND+'-common' and package != BLEND+'-tasks' and package != BLEND+'-config':
                        if package == 'med-dent':
                            package = 'med-dental'
                        if not package.replace(BLEND+'-', '') in tasks_found:
                            tasks_found.append(package.replace(BLEND+'-', ''))
                    recommends = depdata[package]['recommends']
                    task.add_recommends(package,recommends)
                    # in previous package versions we use Depends rather then Recommends
                    if 'depends' in depdata[package]:
                        depends = depdata[package]['depends']
                        task.add_recommends(package,depends)

    if debug > 0:
        for t in tasks:
            print("%s: %s" % (t, tasks[t]))
    if not tasks_found:
        print("No tasks found for Blend %s." % BLEND, file=stderr)
        exit()
    tasks_found.sort()

    drawlist = TasksGetDrawList(tasks, DRAWTASKS[BLEND], SUPPRESSTASKS[BLEND])
    if debug > 0:
        print(drawlist)

    cNorm  = colors.Normalize(vmin=0, vmax=len(drawlist))
    jet = plt.get_cmap('jet')
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)
    cs = []
    for i in range(len(drawlist)):
        colorVal = scalarMap.to_rgba(i)
        cs.append(colorVal)

    xtime = []
    for t in sorted(tasks.keys()):
        task = tasks[t]
        xtime.append(datetime(task.year, task.month, task.day))
    values = []
    for ti in drawlist:
        v = []
        for t in sorted(tasks.keys()):
            task = tasks[t]
            if ti in task.nrecommended:
                v.append(task.nrecommended[ti])
            else:
                # v.append(None)
                v.append(0)
        # print ti, v, len(v)
        values.append(v)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    print(drawlist)
    for z in range(len(drawlist)):
        xs = xtime
        ys = values[z]

        # You can provide either a single color or an array. To demonstrate this,
        # the first bar of each set will be colored cyan.
        color = []
        csz = cs[z]
        for i in range(len(xtime)):
            if ys[i] > 0:
                color.append(csz)
            else:
                color.append((1.0, 1.0, 1.0, 0.0)) # how to make zero dependencies invisible ????
        # col = [cs[z]] * len(xtime)
        # setting width of bars:
        #   http://stackoverflow.com/questions/886716/controling-bars-width-in-matplotlib-with-per-month-data
        ax.bar(xtime, values[z], zs=z, zdir='y', color=color, alpha=0.8, width=90)

    ax.set_yticklabels(drawlist)
    ax.set_xlabel('')
    ax.set_ylabel('')
    ax.set_zlabel('')

    plt.show()


if __name__ == '__main__':
  main()

# vim:set et tabstop=2:
