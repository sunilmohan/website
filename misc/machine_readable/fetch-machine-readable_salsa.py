#!/usr/bin/python3

import gitlab
import os
import sys
import shutil
import base64
import subprocess
import time
from dateutil import parser
import re
import json

BLENDSGROUPS={ '3dprinter'		: '3dprinting-team'
             , 'pkg-a11y'		: 'a11y-team'
             , 'debian-astro'		: 'debian-astro-team'
             , 'debian-edu'		: 'debian-edu'
             , 'debian-med'		: 'med-team'
             , 'debian-science'		: 'science-team'
             , 'debichem'		: 'debichem-team'
             , 'deeplearning'           : 'deeplearning-team'
             , 'neurodebian'		: 'neurodebian-team'
             , 'pkg-electronics'	: 'electronics-team'
             , 'pkg-games'		: 'games-team'
             , 'pkg-go'                 : 'go-team'
             , 'pkg-grass'		: 'debian-gis-team'
             , 'pkg-hamradio'		: 'debian-hamradio-team'
             , 'pkg-iot'		: 'debian-iot-team'
             , 'pkg-java'		: 'java-team'
             , 'pkg-js'			: 'js-team'
             , 'pkg-multimedia'		: 'multimedia-team'
             , 'pkg-octave'		: 'pkg-octave-team'
             , 'pkg-perl'		: 'perl-team'
             , 'pkg-phototools'		: 'debian-phototools-team'
             , 'pkg-r'			: 'r-pkg-team'
             , 'pkg-voip'		: 'pkg-voip-team'
             , 'python-team'		: 'python-team'
             , 'tryton'			: 'tryton-team'
             }

# Do not parse these projects which are no real packages
BLACKLIST = [  '1epic'
             , 'AliothRewriter'
             , 'bio-linux'
             , 'communication'
             , 'helper-scripts'
             , 'maintenance-utilities'
             , 'package_template'
             , 'papers'
             , 'perl.debian.net'
             , 'policy'
             , 'r-bioc-000-dependency-scheme'
             , 'scripts'
             , 'test'
             , 'website'
            ]

# FIXME: Do not assume package name is path name on Salsa (-> there are pathes like "Cave Story NX")

# just start with one team
# BLENDSGROUPS={ 'debian-med'		: 'med-team'}

debianmetadata = [ 'changelog',
                   'control',
                   'copyright',
                   'README.Debian'
                 ]
upstreammetadata = [ 'edam',
                     'metadata'
                   ]

TDNAME='machine-readable'
MACHINEREADABLEDIR='/srv/blends.debian.org/www/_'+TDNAME
MACHINEREADABLEARCHIVE=os.path.join(MACHINEREADABLEDIR, TDNAME+'.tar.xz')
READMEDEBIANARCHIVE=os.path.join(MACHINEREADABLEDIR, 'README.Debian.tar.xz')
TARGETDIR=os.path.join(os.environ['HOME'],TDNAME)
JSONPATH=os.path.join(TARGETDIR,'.data')
JSON=os.path.join(JSONPATH, TDNAME+'_data.json')
jsondata=[] # project, source, blend, path, status

SLEEPTIMEFILES=5	# Wait 5 seconds until next file is fetched
SLEEPTIMEPROJECTS=60	# Wait 60 seconds until next group is queried for all projects
SLEEPUNTILRETRY=600	# In case of overloaded Salsa wait for 10min
debug=1

# FIXME: Find a method to detect removed repositories and clean this up here
#        Keep all projects in a json file and compare with current projects
try:
    os.makedirs(TARGETDIR)
except:
    pass

def output_metadata(project, subdir, metadata):
    try:
        items = project.repository_tree(path=subdir, recursive=False)
    except gitlab.exceptions.GitlabGetError as err:
        if "403 Forbidden" in str(err): 
            print("%s/%s has restricted access to dir %s - will not parse" % (gpath, name, subdir), file=sys.stderr)
            return None
        print("%s/%s does not seem to have dir %s (%s)" % (gpath, name, subdir, str(err)), file=sys.stderr)
        return None
    except:
        branches = project.branches.list()
        sid_branch = 'debian/sid'
        if sid_branch in branches:
            try:
                items = project.repository_tree(path=subdir, recursive=False, ref=sid_branch)
            except:
                print("%s/%s does not seem to have dir %s. PLEASE VERIFY THIS SINCE IT IS SUSPICIOUS! Even Branch %s was inspected." % (gpath, name, subdir, sid_branch), file=sys.stderr)
        else:
            otherbranches = []
            for b in branches:
                if b in ['master', 'pristine-tar', 'upstream', sid_branch]:
                     continue
                otherbranches.append(b)
            print("%s/%s does not seem to have dir %s. PLEASE VERIFY THIS SINCE IT IS SUSPICIOUS! Available branches are: %s" % (gpath, name, subdir, str(otherbranches)), file=sys.stderr)
            return None
#        print("To be sure try again ... after sleeping %i" % SLEEPTIMEPROJECTS)
#        time.sleep(SLEEPTIMEPROJECTS)
#        project2 = gl.projects.get(gpath+'%2F'+name, lazy=True)
#        try:
#            items2 = project.repository_tree(path=subdir, recursive=False)
#            items = items2
#            print("Succeeded after second try seeking for dir %s in %s/%s" % (subdirm, gpath, name))
#        except gitlab.exceptions.GitlabGetError as err:
#            print("%s/%s does not seem to have dir %s (%s)" % (gpath, name, subdir, str(err)), file=sys.stderr)
#            return None
    for item in items:
        if item['name'] in metadata:
            file_info = project.repository_blob(item['id'])
            content = base64.b64decode(file_info['content'])
            if item['name'] == 'metadata':
                ext = '.upstream'
            else:
                ext = '.'+item['name']
            with open(os.path.join(namedir,name+ext), 'wb') as out:
                out.write(content)
            out.close()
    return True

def get_projects_of_subgroups(group):
    more_projects = []
    valid_names = []
    subgroups = group.subgroups.list(all=True, order_by='name', sort='asc', simple=True)
    if subgroups:
        for subgroup in subgroups:
            # print("id = %i, name = %s, full_path = %s" % ( subgroup.id, subgroup.name, subgroup.full_path))
            # print(subgroup)
            if subgroup.name not in valid_names:
                valid_names.append(subgroup.name)
            real_group = gl.groups.get(subgroup.id, lazy=True)
            sprojects = real_group.projects.list(all=True, order_by='name', sort='asc', simple=True)
            for project in sprojects:
                if project.name not in BLACKLIST :
                    more_projects.append(project)
            # call recursively for subdirs
            (sprojects, valid_subgroup_names) =  get_projects_of_subgroups(real_group)
            for project in sprojects:
                if project.name not in BLACKLIST :
                    # reset group name to main group
                    project.namespace['name'] = group.name
                    more_projects.append(project)
    return ( more_projects, valid_names )

# SALSA_TOKEN=os.environ['SALSA_TOKEN']

gl = gitlab.Gitlab("https://salsa.debian.org") # , private_token=SALSA_TOKEN) # anonymous access is fine
#gl = gitlab.Gitlab.from_config()  # alternatively with data in .python-gitlab.cfg

LAST_ACTIVITY='last_activity_at: '
LAST_ACTIVITY_LENGTH=len(LAST_ACTIVITY)

for blend, gpath in sorted(BLENDSGROUPS.items()):
    unchanged, changed, new, ignored = (0, 0, 0, 0)
    group=gl.groups.get(gpath, with_projects=False, simple=True)
    valid_group_names = [group.name]
    try:
        projects = group.projects.list(all=True, order_by='name', sort='asc', simple=True)
    except gitlab.exceptions.GitlabGetError as err:
        print("GitlabGetError: %s when parsing group %s - wait %s seconds", (str(err), gpath, SLEEPUNTILRETRY))
        time.sleep(SLEEPUNTILRETRY)
        projects = group.projects.list(all=True, order_by='name', sort='asc', simple=True)
        # If Salsa is too busy do not try again
    except gitlab.exceptions.GitlabHttpError as err:
        print("GitlabHttpError: %s when parsing group %s - wait %s seconds", (str(err), gpath, SLEEPUNTILRETRY))
    # DEBUG : only few projects to be faster
    # projects = group.projects.list(page=1, per_page=10, order_by='name', sort='asc')
    print("%s has %i projects (Blend name %s)" % (group.name, len(projects), blend)) #  group.attributes['id'], group.attributes['path']) # , group.attributes['description'], group.attributes['full_name'])
    (sprojects, valid_subgroup_names) =  get_projects_of_subgroups(group)
    if sprojects:
        print("%i projects found in subgroups" % len(sprojects))
        valid_group_names.extend(valid_subgroup_names)
    projects.extend(sprojects)
    source=''
    for project in projects:
        jdata = {}
        name = project.name
        jdata['blend']   = blend
        # FIXME: name can be any string with capital letters and spaces.  Using this as file name in machine readable files is simply wrong
        jdata['project'] = name
        jdata['path']    = project.path_with_namespace
        # bnd was parsed twice in 'Debian Java Maintainers'
        if project.namespace['name'] not in valid_group_names :
            print("Project %s is in group '%s' and thus not in the group '%s' which is parsed here -> ignored" % (name, project.namespace['name'], group.name))
            ignored += 1
            jdata['status'] = 'ignore'
            jsondata.append(jdata)
            continue
        if name in BLACKLIST or name.endswith('.pages.debian.net'):
            ignored += 1
            jdata['status'] = 'ignore'
            jsondata.append(jdata)
            if debug > 0:
                print("Ignore project %s of blend %s." % (name, blend))
            continue
        namedir = os.path.join(TARGETDIR, name[0])
        if not os.path.exists(namedir):
            os.makedirs(namedir)
        dosomething=True
        try:
            # try reading file - if not exists its just written
            invcs = open(os.path.join(namedir,name+'.vcs'), 'r')
            stored_team=''
            for line in invcs.readlines():
                if line.startswith('Vcs-Browser: https://salsa.debian.org/'):
                    # check in what team space the project was stored
                    stored_team = re.sub('Vcs-Browser: https://salsa.debian.org/([^/]+)/.*','\\1',line.strip())
                    if stored_team != gpath:
                        print("Currently team %s is parsed but project %s was previously found and stored in team %s" % (gpath, name, stored_team))
                if line.startswith('Source: '):
                    jdata['source'] = line[len('Source: '):].strip()
                if line.startswith(LAST_ACTIVITY):
                    last_activity_at = line[LAST_ACTIVITY_LENGTH:].strip()
                    if last_activity_at == project.last_activity_at:
                        dosomething=False
                        unchanged += 1
                        jdata['status'] = 'unchanged'
                        if debug > 1:
                            print("Do nothing in %s since found last_activity_at %s == repository %s" % (name, last_activity_at, project.last_activity_at))
                    else:
                        last_activity_recorded = parser.parse(last_activity_at)
                        last_activity_project  = parser.parse(project.last_activity_at)
                        if ( last_activity_project < last_activity_recorded ):
                            print("For some strange reason the repository of %s has an older timestamp (%s) than it was recorded before (%s).  Do not parse again!" % (name, project.last_activity_at, last_activity_at))
                            dosomething=False
                            jdata['status'] = 'unchanged'
                            unchanged += 1
                        if debug > 0:
                            print("Continue with %s since found last_activity_at %s != repository %s" % (name, last_activity_at, project.last_activity_at))
                        jdata['status'] = 'changed'
                        changed += 1
                invcs.close()
        except FileNotFoundError:
            jdata['status'] = 'new'
            new += 1
            pass
        if not dosomething:
            jsondata.append(jdata)
            continue
        if debug > 1:
            print("Writing %s." % os.path.join(namedir,name+'.vcs'))
        # print(name)
	# be friendly to Salsa and add some delay time before real reading of data
        # see thread around https://lists.debian.org/debian-devel/2018/07/msg00125.html
        time.sleep(SLEEPTIMEFILES)
        # for lazy=True read: https://python-gitlab.readthedocs.io/en/stable/api-usage.html#lazy-objects
        pr = gl.projects.get(project.id, lazy=True)
        if output_metadata(pr, 'debian', debianmetadata):
            output_metadata(pr, 'debian/upstream', upstreammetadata)
        try:
            with open(os.path.join(namedir,name+'.changelog'), 'r') as chlog:
                source = chlog.readline().strip().split(' ')[0]
            if source != name:
                print("Warning: package name of project found at %s/%s is different than dir: %s" % (gpath, name, source))
        except FileNotFoundError:
            pass # If no changelog file was created this project is irrelevant anyway
        with open(os.path.join(namedir,name+'.vcs'), 'w') as out:
            out.write("Vcs-Browser: %s\n" % (project.web_url))
            out.write("Vcs-Git: %s.git\n" % (project.web_url))
            out.write("Blend: %s\n" % blend)
            if source:
                out.write("Source: %s\n" % source)
                jdata['source'] = source
            out.write("%s%s\n" % (LAST_ACTIVITY, project.last_activity_at))
        out.close()
        jsondata.append(jdata)
    print("In %s %i repositories changed, %i remained unchanged, %i were new and %i were ignored." % (blend, changed, unchanged, new, ignored))
    time.sleep(SLEEPTIMEPROJECTS)

try:
    os.makedirs(MACHINEREADABLEDIR)
except:
    pass
p = subprocess.Popen(['tar', '--exclude=*README.Debian', '--exclude='+JSON, '-caf', MACHINEREADABLEARCHIVE, TDNAME], cwd=os.environ['HOME'])
p.wait()
p = subprocess.Popen(['tar', '--exclude=.data/*', '--exclude=./machine-readable/.data/', '--exclude=*.control', '--exclude=*.changelog', '--exclude=*.upstream', '--exclude=*.edam', '--exclude=*.vcs', '--exclude=*.copyright', '--exclude='+JSON, '-caf', READMEDEBIANARCHIVE, TDNAME], cwd=os.environ['HOME'])
p.wait()

try:
    os.makedirs(JSONPATH)
except:
    pass
with open(JSON, 'w') as outfile:
    json.dump(jsondata, outfile)
