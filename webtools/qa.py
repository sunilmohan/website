#!/usr/bin/python3
# Copyright 2013: Andreas Tille <tille@debian.org>
# License: GPL

import codecs
from sys import argv, stderr, exit
import os
import psycopg2
import json
import re
import time
from datetime import date, datetime
from email.utils import formatdate
import email.utils
import gettext

from genshi.template import TemplateLoader
from genshi import Markup

from blendstasktools import ReadConfig, RowDictionaries, CheckOrCreateOutputDir, SetFilePermissions, _execute_udd_query, curs

SLOWQUERYREPORTLIMIT = 30

debug = 1

def main():

    if len(argv) <= 1:
        stderr.write("Usage: %s <Blend name>\n" % argv[0])
        exit(-1)

    blendname = argv[1]
    config = ReadConfig(blendname)

    query = """
CREATE TEMPORARY VIEW autopkgtest_fail ( source, suite_arch, messages ) AS
  SELECT source, suite_arch, messages FROM (
    SELECT source, string_agg(suite_arch, ', ') AS suite_arch , array_agg(message) AS messages
     FROM (
       SELECT source, string_agg(suite || '_' || arch, ', ') AS suite_arch , message
         FROM ci
         WHERE status = 'fail'
         GROUP BY source, message
     ) tmp1 GROUP BY source
  ) tmp2
  WHERE suite_arch LIKE '%testing%' OR suite_arch LIKE '%unstable%' ; -- Make sure not only neutral test in stable will raise a singnal here

CREATE TEMPORARY VIEW autopkgtest_neutral ( source, suite_arch, messages ) AS
  SELECT source, suite_arch, messages FROM (
    SELECT source, string_agg(suite_arch, ', ') AS suite_arch , array_agg(message) AS messages
     FROM (
       SELECT source, string_agg(suite || '_' || arch, ', ') AS suite_arch , message
         FROM ci
         WHERE status = 'neutral'
         GROUP BY source, message
     ) tmp1 GROUP BY source
  ) tmp2
  WHERE suite_arch LIKE '%testing%' OR suite_arch LIKE '%unstable%' ; -- Make sure not only neutral test in stable will raise a singnal here
"""
    _execute_udd_query(query)

    query = """PREPARE query_qa_packages (text) AS
      SELECT DISTINCT p.source, t.tasks,
             ci.status_agg,
             CASE WHEN ts.testsuite IS NULL THEN 'missing' ELSE ts.testsuite END AS testsuite,
             failci.suite_arch AS failed_suites_arch,
             failci.messages   AS failed_messages,
             neutralci.suite_arch AS neutral_suites_arch,
             neutralci.messages   AS neutral_messages,
             COALESCE (ar.version, 'none') AS autoremoval_version, ar.bugs,
             CAST (to_timestamp(ar.first_seen::numeric) AS date) AS first_seen,
             CAST (to_timestamp(ar.last_checked::numeric) AS date) AS last_checked,
             CAST (to_timestamp(ar.removal_time::numeric) AS date) AS removal,
             rdeps AS autoremoval_rdeps, buggy_deps, bugs_deps, rdeps_popcon,
             m.in_testing, m.testing_version,
             m.in_unstable, m.unstable_version,
             m.in_unstable-m.in_testing as days_to_migrate,
             -- excuses
             me.migration_policy_verdict,
             me.old_version AS excuses_old_version, me.new_version AS excuses_new_version, me.is_candidate AS excuses_is_candidate,
             me.reason, me.dependencies AS excuses_dependencies, me.invalidated_by_other_package,
             -- piuparts
             piu.piuparts_fail, piu.version AS piuparts_version
        FROM packages p
        -- begin tests
        LEFT OUTER JOIN (SELECT source, array_agg(status) AS status_agg FROM (
                            SELECT DISTINCT source, status FROM (
                                SELECT ci.source, ci.suite, ci.arch, ci.status, ci.version, regexp_replace(s.version, E'\\\\+b[0-9]+$', '') AS rel_version FROM ci ci
                                LEFT JOIN (SELECT source, CASE WHEN release='sid' THEN 'unstable'
                                                               WHEN release='bullseye' THEN 'testing'
                                                               ELSE release END AS suite,
                                                  architecture AS arch, version FROM packages) s ON ci.source=s.source AND ci.suite=s.suite AND (ci.arch=s.arch OR s.arch='all') ) tmp
                                           WHERE (version >= rel_version OR rel_version IS NULL) AND status != 'pass' AND suite IN ('testing', 'unstable') AND version != 'n/a'
                        ) tmp GROUP BY source) ci ON p.source = ci.source
        LEFT OUTER JOIN autopkgtest_fail failci ON p.source = failci.source
        LEFT OUTER JOIN autopkgtest_neutral neutralci ON p.source = neutralci.source
        LEFT OUTER JOIN (SELECT source, testsuite FROM (SELECT DISTINCT source, testsuite, row_number() OVER (PARTITION BY source ORDER BY version DESC) FROM sources WHERE release='sid') tmp WHERE row_number = 1) ts ON p.source = ts.source
        LEFT OUTER JOIN (SELECT source, gui FROM (
                            SELECT source, gui, row_number() OVER (PARTITION BY source ORDER BY gui DESC) FROM (
                                SELECT DISTINCT p.package, p.source, gui FROM packages p LEFT JOIN (
                                    SELECT package, 'gui' AS gui FROM debtags WHERE tag LIKE 'uitoolkit::%' OR tag LIKE '%x11%' GROUP BY package
                                ) t ON p.package = t.package WHERE release ='sid' ) dt ) st WHERE row_number = 1) tags ON p.source = tags.source
        -- end tests
        LEFT OUTER JOIN testing_autoremovals ar ON p.source = ar.source
        LEFT OUTER JOIN migrations m ON p.source = m.source
        LEFT OUTER JOIN migration_excuses me ON p.source = me.source
        LEFT OUTER JOIN (SELECT source, piuparts_fail, version FROM
                          (SELECT array_agg(section) AS piuparts_fail, source, version, row_number() OVER (PARTITION BY source ORDER BY version DESC)
                           FROM piuparts_status WHERE status = 'fail' GROUP BY version, source) tmp1 WHERE row_number = 1) piu ON p.source = piu.source
        LEFT OUTER JOIN (SELECT source, array_agg(task) AS tasks FROM
                          (SELECT DISTINCT ps.source, bd.task FROM blends_dependencies bd
                             INNER JOIN (SELECT DISTINCT package, source FROM packages WHERE release = 'sid') ps ON bd.package = ps.package
                             WHERE blend = $1) tmp GROUP BY source
                        ) t ON p.source = t.source
        WHERE p.package IN (
          SELECT package FROM blends_dependencies WHERE blend = $1
        )
        AND
          ( removal_time IS NOT NULL
            OR
            testing_version != unstable_version
            OR
            (me.item_name != ''
             AND
             me.migration_policy_verdict not in ('PASS', 'REJECTED_TEMPORARILY')
             AND
             p.release = 'sid'
            )
            OR
            piu.piuparts_fail IS NOT NULL
            OR
            ((ci.status_agg IS NOT NULL OR ts.testsuite IS NULL)
              AND tags.gui IS NULL
            )
          )
        AND p.release = 'sid'
    """
    _execute_udd_query(query)

    # What tasks are involved
    query = """PREPARE query_get_tasks (text) AS
      SELECT task, title, description, long_description FROM blends_tasks WHERE blend = $1 ORDER BY task;
    """
    _execute_udd_query(query)

    # initialise bugs_data dictionary for all tasks
    _execute_udd_query("EXECUTE query_get_tasks('%s')" % blendname)
    qa_data = {}
    if curs.rowcount > 0:
        for t in RowDictionaries(curs):
            task = t['task']
            qa_data[task] = {}
            qa_data[task]['title']            = t['title']
            qa_data[task]['description']      = t['description']
            qa_data[task]['long_description'] = t['long_description']
            qa_data[task]['autoremovals']     = 0
            qa_data[task]['migrations']       = 0
            qa_data[task]['autopkgtests']     = 0
            qa_data[task]['data']             = []
    else:
        stderr.write("No tasks metadata received for Blend %s\n" % blendname)
        exit(1)

    # Fetch bugs of all Blends dependencies and store them in a dictionary
    _execute_udd_query("EXECUTE query_qa_packages('%s')" % blendname)
    qa_issues = {}
    if curs.rowcount > 0:
        for qa in RowDictionaries(curs):
            data = {}
            data['source'] = qa['source']
            # Autopkgtests (ci)
            data['ci'] = {}
            if qa['status_agg']:
                data['ci']['status'] = qa['status_agg'] # FIXME: resolve different status
                for task in qa['tasks']:
                    qa_data[task]['autopkgtests'] += 1
            data['ci']['testsuite'] = qa['testsuite']
            data['ci']['css'] = 'ci_missing'
            data['ci']['failed_suites_arch'] = []

            data['ci']['neutral_suites_arch'] = []
            if qa['neutral_suites_arch'] :
                for sa in qa['neutral_suites_arch'].split(", "):
                    (suite,arch) = sa.split("_")
                    data['ci']['neutral_suites_arch'].append({'suite':suite, 'arch':arch})
            data['ci']['neutral_messages'] = []
            if qa['neutral_messages']:
                for msg in qa['neutral_messages']:
                    data['ci']['neutral_messages'].append(msg)
                data['ci']['css'] = 'ci_neutral'

            if qa['failed_suites_arch'] :
                for sa in qa['failed_suites_arch'].split(", "):
                    (suite,arch) = sa.split("_")
                    data['ci']['failed_suites_arch'].append({'suite':suite, 'arch':arch})
            data['ci']['failed_messages'] = []
            if qa['failed_messages']:
                for msg in qa['failed_messages']:
                    data['ci']['failed_messages'].append(msg)
                data['ci']['css'] = 'ci_fail'

            # Autoremovals (ar)
            data['ar'] = {}
            data['ar']['buggy_deps'] = []
            data['ar']['bugs_deps'] = []
            data['ar']['bugs'] = []
            if qa['autoremoval_version'] :
                if qa['autoremoval_version'] == 'none':
                    qa['autoremoval_version'] = None
                    data['ar']['autoremoval_version'] = 'none'
            if qa['autoremoval_version'] :
                for arfeature in ('autoremoval_version', 'first_seen', 'last_checked', 'removal', 'rdeps_popcon', \
                                  'in_testing', 'testing_version', 'in_unstable', 'unstable_version', 'days_to_migrate' ):
		    # FIXME: in_testing, testing_version, in_unstable, unstable_version, days_to_migrate
		    #        are available for all packages not only those with issues but here we keep these only for affected packages
                    if type(qa[arfeature]) is date:
                        # Avoid "Object of type date is not JSON serializable"
                        data['ar'][arfeature] = str(qa[arfeature])
                    else:
                        data['ar'][arfeature] = qa[arfeature]
                if qa['bugs'] :
                    data['ar']['bugs'].append(qa['bugs'])
                if qa['autoremoval_rdeps']:  # FIXME
                    data['ar']['autoremoval_rdeps'] = qa['autoremoval_rdeps']
                if qa['buggy_deps']:
                    for bd in qa['buggy_deps'].split(","):
                        data['ar']['buggy_deps'].append(bd)
                if qa['bugs_deps']:  # FIXME
                    for bd in qa['bugs_deps'].split(","):
                        data['ar']['bugs_deps'].append(bd)
                for task in qa['tasks']:
                    qa_data[task]['autoremovals'] += 1

            # Excuses (ex)
            data['ex'] = {}
            if qa['migration_policy_verdict'] :
                data['ex']['migration_policy_verdict'] = qa['migration_policy_verdict']
                data['ex']['excuses_old_version'] = qa['excuses_old_version']
                data['ex']['excuses_new_version'] = qa['excuses_new_version']
                data['ex']['excuses_is_candidate'] = qa['excuses_is_candidate']	# boolean
                data['ex']['reason'] = qa['reason']				# array
                data['ex']['excuses_dependencies'] = qa['excuses_dependencies'] # dictionary
                data['ex']['invalidated_by_other_package'] = qa['invalidated_by_other_package']	# boolean
                data['ex']['piuparts_fail'] = qa['piuparts_fail']				# array
                data['ex']['piuparts_version'] = qa['piuparts_version']
                for task in qa['tasks']:
                    qa_data[task]['migrations'] += 1
            else:
                data['ex']['migration_policy_verdict'] = ''

            for task in qa['tasks']:
                 qa_data[task]['data'].append(data)

    else:
        stderr.write("No information about QA features received for Blend %s\n" % blendname)
        exit(1)

    for task in qa_data:
        print(task+": autopkgtests=%(autopkgtests)s, migrations=%(migrations)s, autoremovals=%(autoremovals)s" % (qa_data[task]))

    # Define directories used
    current_dir  = os.path.dirname(__file__)
    # locale_dir   = os.path.join(current_dir, 'locale')
    template_dir = os.path.join(current_dir, 'templates')

    # initialize gensi
    loader = TemplateLoader([template_dir], auto_reload=True,
                            default_encoding="utf-8")

    outputdir = CheckOrCreateOutputDir(config['outputdir'], 'qa')  # FIXME: as long as we are not finished use different dir
    if outputdir is None:
        exit(-1)

    t = datetime.now()

    # Initialize i18n
    domain = 'blends-webtools'
    gettext.install(domain)

    data = {}
    data['projectname'] = blendname
    data['qa_data']   = qa_data
    if config.get('advertising') is not None:
        # we have to remove the gettext _() call which was inserted into the config
        # file to enable easy input for config file editors - but the call has to
        # be made explicitely in the python code
        advertising = re.sub('_\(\W(.+)\W\)', '\\1', config['advertising'])
        # gettext needs to escape '"' thus we need to remove the escape character '\'
        data['projectadvertising'] = Markup(re.sub('\\\\"', '"', advertising))
    else:
        data['projectadvertising'] = None

    data['summary']           = _('Summary')
    data['idxsummary']        = _("""A %sDebian Pure Blend%s is a Debian internal project which assembles
a set of packages that might help users to solve certain tasks of their work.  The list on
the right shows the tasks of %s.""") \
        % ('<a href="https://blends.debian.org/">', '</a>', data['projectname'])
    data['idxsummary']        = Markup(data['idxsummary'])

    t = datetime.now()
    data['lang']              = 'en'
    data['othertasks']        = _("Links to other tasks")
    data['taskslink']         = _("Tasks")
    data['qalink']            = _("Tasks overview")
    data['legend']            = _("Legend")
    data['qaoftask']          = _("QA overview of task")
    data['totalautoremovals'] = _("Total autoremovals")
    data['totalmigrations']   = _("Total migrations")
    data['totalautopkgtests'] = _("Total autopkgtests")
    data['summary']           = _('Summary')
    data['qasummary']       = _("""A %sDebian Pure Blend%s is a Debian internal project which assembles
    a set of packages that might help users to solve certain tasks of their work.  This page should be helpful
    to track down several quality assurance measures of packages that are interesting for the %s project to
    enable developers a quick overview about possible problems.""") \
        % ('<a href="https://blends.debian.org/">', '</a>', data['projectname'])
    data['qasummary']        = Markup(data['qasummary'])
    data['gtstrQaPage']      = _("QA page")
    data['gtstrListOfQapages'] = _("This is a list of metapackages.  The links are leading to the respective QA page.")
    data['updatetimestamp']   = _('Last update:') + ' ' + formatdate(time.mktime(t.timetuple()))

    # Debuging output in JSON file
    if debug > 0:
        with open(blendname + '_qa.json', 'w') as f:
            if debug > 1:
                for task in qa_data:
                    f.write("*** %s ***\n" % task)
                    for status in STATES:
                        if status in qa_data[task]:
                            f.write("%s\n" % status)
                            f.write(json.dumps(qa_data[task][status]))
                            f.write("\n\n")
            f.write(json.dumps(qa_data))
        SetFilePermissions(blendname + '_qa.json')


### FIXME: continue here

    data['headings'] = {
        'dependent' : _('Open QA issues in dependent packages'),
        'suggested' : _('Open QA issues in suggested packages'),
    }
    data['cssclass'] = {
        'dependent' : 'qadependent',
        'suggested' : 'qasuggested',
    }
    # FIXME: just always use 'depends' or 'dependent' etc.  This translation is just to be able to compare with old output
    data['category'] = {
        'depends'   : 'dependent',
        'suggests'  : 'suggested',
    }

    for key in ('css', 'homepage', 'projecturl', 'projectname', 'logourl', 'ubuntuhome', 'projectubuntu'):
        data[key] = config[key]

    for task in qa_data:
        data['task']            = task

        template = loader.load('qa.xhtml')
        with codecs.open(outputdir + '/' + task + '.html', 'w', 'utf-8') as f:
            f.write(template.generate(**data).render('xhtml'))
        SetFilePermissions(outputdir + '/' + task + '.html')

    template = loader.load('qa_idx.xhtml')
    outputfile = outputdir + '/index.html'
    with codecs.open(outputfile, 'w', 'utf-8') as f:
        f.write(template.generate(**data).render('xhtml'))
    SetFilePermissions(outputfile)

if __name__ == '__main__':
    main()
