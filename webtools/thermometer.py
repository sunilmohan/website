#!/usr/bin/python3
# Copyright 2013: Andreas Tille <tille@debian.org>
# License: GPL

from sys import argv, stderr, exit
import codecs
import os
import psycopg2
import json
import re
import time
from datetime import datetime
from email.utils import formatdate
import gettext

from genshi.template import TemplateLoader
from genshi import Markup
from genshi.template.eval import UndefinedError

from blendstasktools import ReadConfig, RowDictionaries, CheckOrCreateOutputDir, SetFilePermissions

# PORT = 5441
UDDPORT = 5452
PORT = UDDPORT
DEFAULTPORT = 5432

###########################################################################################
connection_pars = [
    {'host': 'localhost', 'port': PORT, 'user': 'guest', 'database': 'udd'},
    {'service': 'udd'},
    {'host': 'localhost', 'port': DEFAULTPORT, 'user': 'guest', 'database': 'udd'},

    # Hmmm, I observed a really strange behaviour on one of my machines where
    # connecting to localhost does not work but 127.0.0.1 works fine.  No idea
    # why ... but this should do the trick for the moment
    {'host': '127.0.0.1', 'port': DEFAULTPORT, 'user': 'guest', 'database': 'udd'},

    # Public UDD mirror as last resort
    {'host': 'public-udd-mirror.xvm.mit.edu', 'port': 5432,
     'user': 'public-udd-mirror', 'password': 'public-udd-mirror', 'database': 'udd'},
]
conn = None
for par in connection_pars:
    try:
        conn = psycopg2.connect(**par)
        break
    except psycopg2.OperationalError as err:
        continue

if conn is None:
    raise err

conn.set_client_encoding('utf-8')
curs = conn.cursor()
# uddlog = open('logs/uddquery.log', 'w')

def _execute_udd_query(query):
    try:
        curs.execute(query)
    except psycopg2.ProgrammingError as err:
        stderr.write("Problem with query\n%s\n%s\n" % (query, str(err)))
        exit(-1)
    except psycopg2.DataError as err:
        stderr.write("%s; query was\n%s\n" % (str(err), query))


def main():

    if len(argv) <= 1:
        stderr.write("Usage: %s <Blend name>\n" % argv[0])
        exit(-1)

    blendname = argv[1]
    config = ReadConfig(blendname)

    # obtain release names automatically
    query = "SELECT release FROM ubuntu_packages WHERE NOT release LIKE '%-%' \
             AND release not in ('lucid', 'precise', 'saucy', 'trusty', 'utopic', 'vivid', 'wily', 'xenial', 'yakkety', 'zesty') \
             GROUP BY release ORDER BY release DESC LIMIT 3"
    _execute_udd_query(query)
    if curs.rowcount > 0:
        releasenames = curs.fetchall()
        latestubuntu = releasenames[0][0]
        ubuntuprev1  = releasenames[1][0]
        ubuntuprev2  = releasenames[2][0]
    else:
        stderr.write("Failed to obtain Ubuntu release names.\n")
        exit(1)

    query = """PREPARE query_thermometer (text) AS
     SELECT b.source,
           stable.version AS stable,
           testing.version AS testing,
           unstable.version AS unstable,
           stable_bpo.version AS "stable_bpo",
           experimental.version AS experimental,
           new.version AS "NEW",
           unreleased.version AS "UNRELEASED",
           ubuntuprev2.version AS ubuntuprev2,
           ubuntuprev1.version AS ubuntuprev1,
           latestubuntu.version AS latestubuntu,
           d.upstream_version AS upstream,
           -- map values from former dehs to upstream table values
           CASE WHEN d.status IS NULL OR
                     d.status = '' OR
                     d.status = 'error'											THEN 'none'
                WHEN d.status = 'Newer version available'		OR d.status = 'newer package available'		THEN 'outdated'
                WHEN d.status = 'up to date'										THEN 'upToDate'
                WHEN d.status = 'Debian version newer than remote site'	OR d.status = 'only older package available'	THEN 'newer-in-debian'
                ELSE 'unknown' -- should not occure!
           END AS upstreamstatus,
           homepage,
           wnpp,
           is_in_debian,
           vcs_browser,
           tasks.tasks,
           CASE WHEN stable.version >= unstable.version
                  OR replace(stable_bpo.version, '~bpo.*', '') >= unstable.version THEN 'upToDate'
                WHEN stable.version <  unstable.version
                  OR replace(stable_bpo.version, '~bpo.*', '') <  unstable.version THEN 'debianOutOfDate'
                WHEN stable.version IS NOT NULL AND unstable.version IS NULL THEN 'obsolete'
                WHEN stable.version IS NULL AND testing.version IS NULL AND unstable.version IS NULL AND new.version IS NULL THEN 'unpackaged'
                WHEN new.version IS NULL AND (experimental.version IS NOT NULL OR unreleased.version IS NOT NULL) THEN 'workInProgress'
                WHEN new.version IS NOT NULL THEN 'new'
                ELSE 'unknown' END AS debianstatus,
           CASE WHEN latestubuntu.version >= unstable.version THEN 'upToDate'
                WHEN latestubuntu.version <  unstable.version THEN 'ubuntuOutOfDate'
                WHEN stable.version IS NOT NULL AND unstable.version IS NULL THEN 'obsolete'
                WHEN stable.version IS NULL AND testing.version IS NULL AND unstable.version IS NULL THEN 'unpackaged'
                WHEN experimental.version IS NOT NULL OR unreleased.version IS NOT NULL THEN 'workInProgress'
                ELSE 'unknown' END AS ubuntustatus
     FROM (
      SELECT DISTINCT p.source, '' AS wnpp FROM packages p
      JOIN blends_dependencies bd ON bd.package = p.package
      JOIN releases r ON p.release = r.release
      WHERE bd.blend = $1 AND
            (r.sort >= (SELECT sort FROM releases WHERE role = 'stable') OR r.sort = 0) -- forget older releases than stable but allow experimental
      UNION
      SELECT DISTINCT n.source, '' AS wnpp FROM new_packages n
      JOIN blends_dependencies bd ON bd.package = n.package
      WHERE bd.blend = $1 AND bd.distribution = 'new'
      UNION
      SELECT DISTINCT u.source, '' AS wnpp FROM ubuntu_packages u
      JOIN blends_dependencies bd ON bd.package = u.package
      WHERE bd.blend = $1 AND bd.distribution = 'ubuntu'
      UNION
      SELECT DISTINCT pr.source, CASE WHEN wnpp!=0 THEN CAST(pr.wnpp AS text) ELSE '' END AS wnpp FROM blends_prospectivepackages pr
      JOIN blends_dependencies bd ON bd.package = pr.package
      WHERE bd.blend = $1 AND bd.distribution = 'prospective'
     ) b
     LEFT OUTER JOIN ( SELECT source, homepage FROM (
      SELECT source, homepage, row_number() OVER (PARTITION BY source ORDER BY version DESC) FROM (
       SELECT DISTINCT p.source, p.homepage, p.version FROM packages p
         JOIN blends_dependencies bd ON bd.package = p.package
         JOIN releases r ON p.release = r.release
         WHERE bd.blend = $1 AND
            (r.sort >= (SELECT sort FROM releases WHERE role = 'stable') OR r.sort = 0) -- forget older releases than stable but allow experimental
       UNION
       SELECT DISTINCT n.source, n.homepage, n.version FROM new_packages n
         JOIN blends_dependencies bd ON bd.package = n.package
         WHERE bd.blend = $1 AND bd.distribution = 'new'
       UNION
       SELECT DISTINCT u.source, u.homepage, u.version FROM ubuntu_packages u
         JOIN blends_dependencies bd ON bd.package = u.package
         WHERE bd.blend = $1 AND bd.distribution = 'ubuntu'
         UNION
       SELECT DISTINCT pr.source, pr.homepage, pr.chlog_version as version FROM blends_prospectivepackages pr
         JOIN blends_dependencies bd ON bd.package = pr.package
         WHERE bd.blend = $1 AND bd.distribution = 'prospective'
       ) hpversion
      GROUP BY source, homepage, version
      ) tmp
      WHERE row_number = 1
     ) homepage ON b.source = homepage.source
     LEFT OUTER JOIN ( SELECT source, vcs_browser FROM (
      SELECT source, vcs_browser, row_number() OVER (PARTITION BY source ORDER BY version DESC) FROM (
       SELECT DISTINCT p.source, s.vcs_browser, p.version FROM packages p
         JOIN blends_dependencies bd ON bd.package = p.package
         JOIN sources s ON p.source = s.source AND p.release = s.release
         JOIN releases r ON p.release = r.release
         WHERE bd.blend = $1 AND
            (r.sort >= (SELECT sort FROM releases WHERE role = 'stable') OR r.sort = 0) -- forget older releases than stable but allow experimental
       UNION
       SELECT DISTINCT np.source, us.vcs_browser, np.version FROM new_packages np
         JOIN blends_dependencies bd ON bd.package = np.package
         JOIN new_sources us ON np.source = us.source
         WHERE bd.blend = $1 AND bd.distribution = 'new'
       UNION
       SELECT DISTINCT up.source, us.vcs_browser, up.version FROM ubuntu_packages up
         JOIN blends_dependencies bd ON bd.package = up.package
         JOIN ubuntu_sources us ON up.source = us.source AND up.release = us.release
         WHERE bd.blend = $1 AND bd.distribution = 'ubuntu'
       UNION
       SELECT DISTINCT pr.source, pr.vcs_browser,  pr.chlog_version as version FROM blends_prospectivepackages pr
         JOIN blends_dependencies bd ON bd.package = pr.package
         WHERE bd.blend = $1 AND bd.distribution = 'prospective'
       ) hpversion
      GROUP BY source, vcs_browser, version
      ) tmp
      WHERE row_number = 1
     ) vcs ON b.source = vcs.source
     LEFT OUTER JOIN (
      SELECT DISTINCT p.source, strip_binary_upload(MAX(s.version)) AS version
        FROM packages p
        JOIN releases r ON p.release = r.release
        JOIN blends_dependencies b ON b.package = p.package
        -- make sure we get the right source version that does not necessarily match binary version
        JOIN sources s ON p.source = s.source AND p.release = s.release
        WHERE b.blend = $1 AND r.role = 'unstable' AND p.distribution = 'debian'
        GROUP BY p.source
        ORDER BY p.source
      ) unstable ON b.source = unstable.source
     LEFT OUTER JOIN (
      SELECT DISTINCT p.source, strip_binary_upload(MAX(s.version)) AS version
        FROM packages p
        JOIN releases r ON p.release = r.release
        JOIN blends_dependencies b ON b.package = p.package
        -- make sure we get the right source version that does not necessarily match binary version
        JOIN sources s ON p.source = s.source AND p.release = s.release
        WHERE b.blend = $1 AND r.role = 'testing' AND p.distribution = 'debian'
        GROUP BY p.source
        ORDER BY p.source
      ) testing ON b.source = testing.source
     LEFT OUTER JOIN (
      SELECT DISTINCT p.source, strip_binary_upload(MAX(s.version)) AS version
        FROM packages p
        JOIN releases r ON p.release = r.release
        JOIN blends_dependencies b ON b.package = p.package
        -- make sure we get the right source version that does not necessarily match binary version
        JOIN sources s ON p.source = s.source AND p.release = s.release
        WHERE b.blend = $1 AND r.role = 'stable' AND p.distribution = 'debian'
        GROUP BY p.source
        ORDER BY p.source
      ) stable ON b.source = stable.source
     LEFT OUTER JOIN (
      SELECT DISTINCT p.source, strip_binary_upload(MAX(s.version)) AS version
        FROM packages p
        JOIN releases r ON p.release = r.release
        JOIN blends_dependencies b ON b.package = p.package
        -- make sure we get the right source version that does not necessarily match binary version
        JOIN sources s ON p.source = s.source AND p.release = s.release
        WHERE b.blend = $1 AND r.role = 'experimental' AND p.distribution = 'debian'
        GROUP BY p.source
        ORDER BY p.source
      ) experimental ON b.source = experimental.source
     LEFT OUTER JOIN (
      SELECT DISTINCT p.source, strip_binary_upload(MAX(s.version)) AS version
        FROM packages p
        JOIN releases r ON p.release = r.release || '-backports'
        JOIN blends_dependencies b ON b.package = p.package
        -- make sure we get the right source version that does not necessarily match binary version
        JOIN sources s ON p.source = s.source AND p.release = s.release
        WHERE b.blend = $1 AND p.release = (SELECT release FROM releases WHERE role='stable') || '-backports' AND p.distribution = 'debian'
        GROUP BY p.source
        ORDER BY p.source
      ) stable_bpo ON b.source = stable_bpo.source
     LEFT OUTER JOIN (
      SELECT DISTINCT p.source, 1 AS is_in_debian FROM packages p
     ) is_in_debian ON b.source = is_in_debian.source
     LEFT OUTER JOIN (
      SELECT DISTINCT np.source, strip_binary_upload(MAX(np.version)) AS version
        FROM new_packages np
        JOIN blends_dependencies b ON b.package = np.package
        -- make sure we get the right source version that does not necessarily match binary version
        JOIN new_sources ns ON np.source = ns.source
        WHERE b.blend = $1 AND b.distribution = 'new'
        GROUP BY np.source
        ORDER BY np.source
     ) new ON b.source = new.source
     LEFT OUTER JOIN ( -- an 'UNRELEASED' version can be due to not yet finished work in VCS or not yet uploaded at all
      SELECT DISTINCT source, version FROM (
       SELECT DISTINCT p.source, strip_binary_upload(MAX(v.version)) AS version
        FROM packages p
        JOIN blends_dependencies b ON b.package = p.package
        -- make sure we get the right source version that does not necessarily match binary version
        JOIN sources s ON p.source = s.source AND p.release = s.release
        JOIN vcs v ON s.source = v.source
        WHERE b.blend = $1 AND v.distribution = 'UNRELEASED'
        GROUP BY p.source
       UNION
       SELECT DISTINCT pr.source, strip_binary_upload(chlog_version) AS version
        FROM blends_dependencies b
        JOIN blends_prospectivepackages pr ON b.package = pr.package
        WHERE b.blend = $1 AND b.distribution != 'new'
       ) tmp
      ) unreleased ON b.source = unreleased.source
     LEFT OUTER JOIN (
      SELECT DISTINCT u.source, strip_binary_upload(MAX(s.version)) AS version
        FROM ubuntu_packages u
        JOIN blends_dependencies b ON b.package = u.package
        JOIN ubuntu_sources s ON u.source = s.source AND u.release = s.release
        WHERE b.blend = $1 AND u.release = '%s'
        GROUP BY u.source
        ORDER BY u.source
      ) ubuntuprev2 ON b.source = ubuntuprev2.source
     LEFT OUTER JOIN (
      SELECT DISTINCT u.source, strip_binary_upload(MAX(s.version)) AS version
        FROM ubuntu_packages u
        JOIN blends_dependencies b ON b.package = u.package
        JOIN ubuntu_sources s ON u.source = s.source AND u.release = s.release
        WHERE b.blend = $1 AND u.release = '%s'
        GROUP BY u.source
        ORDER BY u.source
      ) ubuntuprev1 ON b.source = ubuntuprev1.source
     LEFT OUTER JOIN (
      SELECT DISTINCT u.source, strip_binary_upload(MAX(s.version)) AS version
        FROM ubuntu_packages u
        JOIN blends_dependencies b ON b.package = u.package
        JOIN ubuntu_sources s ON u.source = s.source AND u.release = s.release
        WHERE b.blend = $1 AND u.release = '%s'
        GROUP BY u.source
        ORDER BY u.source
      ) latestubuntu ON b.source = latestubuntu.source
      LEFT OUTER JOIN (
        SELECT source, array_agg(task) AS tasks FROM (
          SELECT DISTINCT p.source, b.task
            FROM all_packages p                                  -- needs 'all_packages' / 'all_sources' and NOT verifying releases to fetch Ubuntu only packages
            JOIN blends_dependencies b ON b.package = p.package
            JOIN all_sources s ON p.source = s.source
            WHERE b.blend = $1
          UNION
          SELECT DISTINCT np.source, b.task
            FROM new_packages np
            JOIN blends_dependencies b ON b.package = np.package
            JOIN new_sources ns ON np.source = ns.source
            WHERE b.blend = $1
          UNION
            SELECT DISTINCT pr.source, bd.task FROM blends_prospectivepackages pr
              JOIN blends_dependencies bd ON bd.package = pr.package
              WHERE bd.blend = $1 AND bd.distribution = 'prospective'
          ) tmp
        GROUP BY source
      ) tasks ON b.source = tasks.source
      LEFT OUTER JOIN (SELECT source, upstream_version, status FROM upstream WHERE release = 'sid') d ON b.source = d.source
      ORDER BY b.source
    ;
    """ % (ubuntuprev2, ubuntuprev1, latestubuntu)
    _execute_udd_query(query)

    _execute_udd_query("EXECUTE query_thermometer('%s')" % blendname)

    if curs.rowcount > 0:
        blend_data = RowDictionaries(curs)
        # f = open(blendname+'_thermometer.json', 'w')
        # print >>f, json.dumps(blend_data)
        # f.close()
        # SetFilePermissions(blendname+'_thermometer.json')
    else:
        stderr.write("No data received for Blend %s\n" % blendname)
        exit(1)

    # Define directories used
    current_dir  = os.path.dirname(__file__)
    # locale_dir   = os.path.join(current_dir, 'locale')
    template_dir = os.path.join(current_dir, 'templates')

    # initialize gensi
    loader = TemplateLoader([template_dir], auto_reload=True,
                            default_encoding="utf-8")

    outputdir = CheckOrCreateOutputDir(config['outputdir'], 'thermometer')
    if outputdir is None:
        exit(-1)

    t = datetime.now()

    # Initialize i18n
    domain = 'blends-webtools'
    gettext.install(domain)

    data = {}
    data['projectname'] = blendname
    data['blend_data']  = blend_data
    if config.get('advertising') != None:
        # we have to remove the gettext _() call which was inserted into the config
        # file to enable easy input for config file editors - but the call has to
        # be made explicitely in the python code
        advertising = re.sub('_\(\W(.+)\W\)', '\\1', config['advertising'])
        # gettext needs to escape '"' thus we need to remove the escape character '\'
        data['projectadvertising'] = Markup(re.sub('\\\\"', '"', advertising))
    else:
        data['projectadvertising'] = None

    legend = [
        ['upToDate',        'Up to date'],
        ['debianOutOfDate', 'Debian stable behind unstable'],
        ['ubuntuOutOfDate', 'Ubuntu behind Debian unstable'],
        ['new',             'Waiting in NEW'],
        ['unpackaged',      'Not packaged'],
        ['obsolete',        'Obsolete'],
        ['newer-in-debian', 'Upstream behind unstable'],
        ['uptodate',        'Unstable fits upstream'],
        ['outdated',        'Unstable behind upstream'],
    ]

    ulegend = legend[:]
    ulegend.remove(['new', 'Waiting in NEW'])

    data['ubuntuprev2']       = ubuntuprev2
    data['ubuntuprev1']       = ubuntuprev1
    data['latestubuntu']      = latestubuntu
    data['legend']            = legend
    data['ulegend']           = ulegend
    data['summary']           = _('Summary')
    data['idxsummary']        = _("""A %sDebian Pure Blend%s is a Debian internal project which assembles
a set of packages that might help users to solve certain tasks of their work.  The list on
the right shows the tasks of %s.""") \
        % ('<a href="https://blends.debian.org/">', '</a>', data['projectname'])
    data['idxsummary']        = Markup(data['idxsummary'])

    for key in ('homepage', 'projecturl', 'projectname', 'logourl', 'ubuntuhome', 'projectubuntu'):
        data[key] = config[key]
    data['updatetimestamp']   = _('Last update:') + ' ' + formatdate(time.mktime(t.timetuple()))

    data['thermometer']  = blendname + '_thermometer.html'
    os.system("ln -sf %s %s/index.html" % (data['thermometer'], outputdir))
    data['uthermometer'] = 'ubuntu_' + blendname + '_thermometer.html'
    outputfile  = outputdir + '/' + data['thermometer']
    uoutputfile = outputdir + '/' + data['uthermometer']

    template = loader.load('thermometer.xhtml')
    with codecs.open(outputfile, 'w', 'utf-8') as f:
        try:
            f.write(template.generate(**data).render('xhtml'))
        except TypeError as err:
            stderr.write("Problem creating thermometer.html.\tMessage: %s\n" % (str(err)))
    SetFilePermissions(outputfile)
    utemplate = loader.load('uthermometer.xhtml')
    with codecs.open(uoutputfile, 'w', 'utf-8') as f:
        try:
            f.write(utemplate.generate(**data).render('xhtml'))
        except TypeError as err:
            stderr.write("Problem creating uthermometer.html.\tMessage: %s\n" % (str(err)))
    SetFilePermissions(uoutputfile)

if __name__ == '__main__':
    main()
