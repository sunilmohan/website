#
# These strings occured in the old *.php files and the translations should be
# preserved for later use.
# Just keep this file
#

#: ../bug_details.tmpl:8 ../bugs.tmpl:7 ../ddtp.tmpl:7 ../ddtp_details.tmpl:8 ../locales.php:7 ../tasks.tmpl:8 ../tasks_idx.tmpl:8
msgid "summary"
msgstr ""

#: ../bug_details.tmpl:12 ../bugs.tmpl:11
msgid "Bugs for package"
msgstr ""

#: ../bug_details.tmpl:40 ../bugs.tmpl:66 ../ddtp.tmpl:30 ../ddtp_details.tmpl:34 ../tasks.tmpl:112 ../tasks_idx.tmpl:29
msgid "Last update"
msgstr ""

#: ../bug_details.tmpl:41 ../bugs.tmpl:67 ../ddtp.tmpl:31 ../ddtp_details.tmpl:35 ../tasks.tmpl:113 ../tasks_idx.tmpl:30
msgid "Please note: this page gets automatically updated twice a day, on 00:00 and 12:00 UTC."
msgstr ""

#: ../bugs.tmpl:19 ../bugs.tmpl:31
msgid "Summary bugs page"
msgstr ""

#: ../ddtp.tmpl:15
msgid "DDTP Statistics"
msgstr ""

#: ../ddtp_details.tmpl:12
msgid "Summary for package"
msgstr ""

#: ../inc/header.inc.php:34
#, php-format
msgid "Help us to see Debian used by medical practicioners and researchers! Join us on the %sSalsa page%s."
msgstr ""

#: ../index.php:7
msgid "information"
msgstr ""

#: ../index.php:11
msgid "Developers please visit our"
msgstr ""

#: ../index.php:12
msgid "Wiki page"
msgstr ""

#: ../index.php:15
msgid ""
"The Debian Med project presents packages that are associated with <ul><li>medicine</li><li>pre-clinical research</li><li>life science.</li></ul> "
"Its developments are mostly focused on three areas for the moment: <ul><li>medical practice</li><li>imaging</li><li>bioinformatics</li></ul>and can "
"be installed directly from every Debian installation."
msgstr ""

#: ../index.php:22
msgid "warning"
msgstr ""

#: ../index.php:24 ../locales.php:26
#, php-format
msgid ""
"Your browser uses language settings that we could not yet provide translations for.<br />If you can spare one to two hours then please consider to "
"help us in translating our pages for your people, too. Instructions are found %shere%s."
msgstr ""

#: ../index.php:29
#, php-format
msgid "Visit the %sLocalization page%s."
msgstr ""

#: ../index.php:37
msgid "pages"
msgstr ""

#: ../index.php:42
msgid "Group policy"
msgstr ""

#: ../index.php:43
msgid "Bugs"
msgstr ""

#: ../index.php:44
msgid "Quality Assurance"
msgstr ""

#: ../index.php:45
msgid "Debian Description Translation Project"
msgstr ""

#: ../index.php:46
msgid "Tasks of our Blend"
msgstr ""

#: ../index.php:47
msgid "SVN repository"
msgstr ""

#: ../index.php:49
msgid "Localizations"
msgstr ""

#: ../index.php:54
msgid "members"
msgstr ""

#: ../index.php:72 ../index.php:92
msgid "Project Administrator"
msgstr ""

#: ../index.php:77 ../index.php:96
msgid "Project Developer"
msgstr ""

#: ../index.php:91
msgid "Green Wheel"
msgstr ""

#: ../index.php:95
msgid "Grey Wheel"
msgstr ""

#: ../index.php:101
msgid "UTC time"
msgstr ""

#: ../index.php:107
msgid "badges"
msgstr ""

#: ../index.php:113
msgid "Valid XHTML 1.1"
msgstr ""

#: ../index.php:118
msgid "Valid CSS 2"
msgstr ""

#: ../index.php:125
msgid "Berkeley Open Infrastructure for Network Computing"
msgstr ""

#: ../index.php:132
msgid "recent activity"
msgstr ""

#: ../index.php:138
msgid "date"
msgstr ""

#: ../index.php:139 ../locales.php:50
msgid "author"
msgstr ""

#: ../index.php:140
msgid "content"
msgstr ""

#: ../index.php:141
msgid "link"
msgstr ""

#: ../index.php:170
msgid "todo"
msgstr ""

#: ../index.php:220
msgid "Please, note that this is a SVN export of our website. It might break during SVN commits."
msgstr ""

#: ../locales.php:11
msgid "Current locale"
msgstr ""

#: ../locales.php:15
msgid "Priority"
msgstr ""

#: ../locales.php:31
#, php-format
msgid "More information on how to contribute to the Debian Med project, can be found in the %sHow to Contribute%s page."
msgstr ""

#: ../locales.php:41
msgid "localization"
msgstr ""

#: ../locales.php:45
msgid "Currently installed locales"
msgstr ""

#: ../locales.php:48
msgid "locale"
msgstr ""

#: ../locales.php:49
msgid "translation status"
msgstr ""

#: ../locales.php:51
msgid "team"
msgstr ""

#: ../locales.php:92
msgid "Add new locale"
msgstr ""

#: ../tasks.tmpl:18
msgid "The list to the right includes various software projects which are of some interest to the Debian Med Project."
msgstr ""

#: ../tasks.tmpl:19
msgid "Currently, only a few of them are available as Debian packages."
msgstr ""

#: ../tasks.tmpl:20
msgid "It is our goal, however, to include all software in Debian Med which can sensibly add to a high quality Debian Pure Blend."
msgstr ""

#: ../tasks.tmpl:31
#, php-format
msgid ""
"If you discover a project which looks like a good candidate for Debian Med to you, or if you have prepared an inofficial Debian package, please do "
"not hesitate to send a description of that project to the %sDebian Med mailing list%s"
msgstr ""
