#!/usr/bin/python3

import codecs

import json
from sys import argv, exit, stderr
import os
import gettext

import time
from datetime import datetime
from email.utils import formatdate

from genshi.template import TemplateLoader
from genshi import Markup

from blendstasktools import Tasks, GetDependencies2Use, pkgstatus, pkgstatus_sortedkeys, UnlockBlendsTools, CheckOrCreateOutputDir, SetFilePermissions
from blendslanguages import languages

if len(argv) <= 1:
    stderr.write('Usage: {0} <Blend name>\n'.format(argv[0])
                 + 'The <Blend name> needs a matching config file '
                 + 'webconf/<Blend name>.conf')
    exit(-1)

# LockBlendsTools() #  logger handler not defined at this moment, needs rethinking ... FIXME
tasks    = Tasks(argv[1])
tasks.GetAllDependencies()
packages = tasks.GetNamesOnlyDict()
tasks.GetAllDependentPackagesOfBlend()

data = tasks.data
data['timestamp'] = formatdate(time.mktime(datetime.now().timetuple()))
data['tasks'] = tasks.GetTaskDescDict()
data['taskskeys'] = tasks.metapackagekeys
data['pkgcount'] = len(tasks.getPackageNames(['official_high', 'official_low',
                                              'non-free', 'experimental']))
data['popconsubmit'] = tasks.popconsubmit or 'unknown'
data['languages'] = languages

# Define directories used
current_dir  = os.path.dirname(__file__)
locale_dir   = os.path.join(current_dir, 'locale')
template_dir = os.path.join(current_dir, 'templates')

# Initialize i18n
domain = 'blends-webtools'
gettext.install(domain)
try:
    l10nstring = dict((lang['ddtp'],
                       gettext.translation(domain, locale_dir,
                                           languages=[lang['ddtp']],
                                           fallback=True))
                      for lang in languages)
    #print(l10nstring)
except UnicodeDecodeError as err:
    stderr.write("Encoding error: %s\n" % str(err))
    stderr.write(l10nstring)
    #  --> NameError: name 'logger' is not defined
    # logger.error(err)
    #exit(-1)
# Translated strings regarding the categorising of dependencies need to
# be translated and because I did not found a working solution to get
# gettext working with genshi all are collected here even if the additional
# attributes to blendstasktools.pkgstatus rather should go blendstasktools.py

# initialize gensi
loader = TemplateLoader([template_dir], auto_reload=True, default_encoding="utf-8")

outputdir = CheckOrCreateOutputDir(tasks.data['outputdir'], 'tasks')
if outputdir is None:
    exit(-1)


class DefaultEncoder(json.JSONEncoder):
    def default(self, obj):
        return obj.__dict__

with open(os.path.join(outputdir, 'tasks.json'), 'w') as fp:
    json.dump(data, fp, indent=4, sort_keys=True, cls=DefaultEncoder)

t = datetime.now()
htaccess = os.path.join(outputdir, '.htaccess')
htafp = open(htaccess, 'w')
htafp.write("DirectoryIndex index index.html\nOptions +MultiViews\n")

use_dependencystatus = GetDependencies2Use()
for lang in languages:
    htafp.write("AddLanguage {htaccess} .{ddtp}\n".format(**lang))
    l10nstring[lang['ddtp']].install()
    try:
        _ = l10nstring[lang['ddtp']].ugettext
    except AttributeError: # Python 3
        _ = l10nstring[lang['ddtp']].gettext
    data['lang']              = lang['ddtp']
    data['license']           = _('License')
    data['version']           = _('Version')
    data['summary']           = _('Summary')
    data['updatetimestamp']   = _('Last update:') + ' ' + formatdate(time.mktime(t.timetuple()))
    data['nopkgavail']        = _('Debian package not available')
    # Description of package in specific packges (same for Depends, Recommends, Suggests)
    data['legend']            = _("For a better overview of the project's availability as a Debian package, each head row has a color code according to this scheme:")
    data['discovery']         = Markup(_(
        """If you discover a project which looks like a good candidate for %s
        to you, or if you have prepared an unofficial Debian package, please do not hesitate to
        send a description of that project to the <a href="mailto:%s">%s mailing list</a>""")
        % (data['projectname'], data['projectlist'], data['projectname']))
    data['bugs']               = _('Bugs')
    data['thermometer']        = _('Thermometer')
    data['maintstats']         = _('Maintainer Stats')
    data['contact']            = _('Contact')
    data['contribute']         = _('Contribute')
    data['description']        = _('Description')
    data['metapackage']        = _('Metapackage')
    data['packages']           = _('Packages')
    data['gtstrTasksPage']     = _('Tasks page')
    data['gtstrProject']       = _('Project')
    data['gtstrThisIsAList']   = _('This is a list of the Tasks %s is made of:') % data['projectname']
    data['langavail']          = _('This page is also available in the following languages:')
    data['howtosetlang']       = _('How to set <a href="%s">the default document language</a>')
    data['howtosetlang']       = Markup(data['howtosetlang'] % ('https://www.debian.org/intro/cn.%s.html' % lang['ddtp']))
    data['nohomepage']         = _('Homepage not available')
    data['translatedesc']      = _('Translate description')
    data['fixtranslation']     = _('Fix translated description')
    data['popconexplanation']  = _('Popularitycontest results: number of people who use this package regularly (number of people who upgraded this package recently) out of')
    data['tableofcontents']    = _('Table of contents')
    data['packagelist']        = _('complete packagelist')
    if data['advertising'] != None:
        # If data['advertising'] is enclosed in _() gettext tries to ask for translations of 'advertising'
        # which makes no sense.  That's why this is masked by an extra string variable
        advertising = data['advertising']
        # data['projectadvertising'] = _(advertising) # Hopefully translation will work this way ...
        # Genshi needs explicite information that it is dealing with an UTF-8 string which should not be changed
        advertising = _(advertising)
        data['projectadvertising'] = Markup(advertising)
    else:
        data['projectadvertising'] = None

    data['packages']          = _('Packages')
    data['idxsummary']        = _('''The {projectname} Pure Blend contains {pkgcount} packages which
        are grouped by metapackages. Each metapackage will cause the
        installation of packages for a specific topic. The following table lists
        the metapackages of {projectname}''').format(**data)

    pkgstatus['official_high']['headline']    = _('Official Debian packages with high relevance')
    # before fiddling around with unicode() read
    # http://www.red-mercury.com/blog/eclectic-tech/python-unicode-fixing-utf-8-encoded-as-latin-1-iso-8859-1/
    pkgstatus['official_high']['pdolinkname'] = _('Official Debian package')
    pkgstatus['official_low'] ['headline']    = _('Official Debian packages with lower relevance')
    pkgstatus['official_low'] ['pdolinkname'] = pkgstatus['official_high']['pdolinkname']
    pkgstatus['non-free']     ['headline']    = _('Debian packages in contrib or non-free')
    pkgstatus['non-free']     ['pdolinkname'] = _('Debian package in contrib/non-free')
    pkgstatus['experimental'] ['headline']    = _('Debian packages in experimental')
    pkgstatus['experimental'] ['pdolinkname'] = _('Debian package in experimental')
    pkgstatus['new']          ['headline']    = _('Debian packages in New queue (hopefully available soon)')
    pkgstatus['new']          ['pdolinkname'] = _('New Debian package')
    pkgstatus['pkgvcs']       ['headline']    = _('Packaging has started and developers might try the packaging code in VCS')
    pkgstatus['pkgvcs']       ['pdolinkname'] = _('Unofficial Debian package')
    pkgstatus['unofficial']   ['headline']    = _('Unofficial packages built by somebody else')
    pkgstatus['unofficial']   ['pdolinkname'] = pkgstatus['pkgvcs']['pdolinkname']
    pkgstatus['prospective']  ['headline']    = _('No known packages available')
    pkgstatus['prospective']  ['pdolinkname'] = _('Debian package not available')
    pkgstatus['wnpp']         ['headline']    = _('No known packages available but some record of interest (WNPP bug)')
    pkgstatus['wnpp']         ['pdolinkname'] = pkgstatus['prospective']['pdolinkname']
    # Ignore/Avoid is only relevant for blends-dev, not webtools
    pkgstatus['ignore']       ['headline']    = 'Should not show up here'
    pkgstatus['prospective']  ['pdolinkname'] = ''
    pkgstatus['avoid']        ['headline']    = 'Should not show up here'
    pkgstatus['prospective']  ['pdolinkname'] = ''
    pkgstatus['unknown']      ['headline']    = 'Should not show up here'  # There must be a bug somewhere
    pkgstatus['prospective']  ['pdolinkname'] = ''

    # Create the index page
    # Make sure that strings that need to be rendered as they are because they might
    # contain several html tags will be marked correctly
    VERBATIM = ('projectadvertising', )
    for verbatim in VERBATIM:
        if verbatim in data and data[verbatim] != None:
            data[verbatim] = Markup(data[verbatim])

    template = loader.load('tasks_idx.xhtml')

    outputfile = os.path.join(outputdir,
                              'index.{0}.html'.format(lang['short']))
    with codecs.open(outputfile, 'w', 'utf-8') as f:
        f.write(template.generate(**data).render('xhtml'))
    SetFilePermissions(outputfile)
    try:
        template = loader.load('%s_idx.xhtml' % tasks.blendname)
        outputfile = os.path.join(tasks.data['outputdir'],
                                  'index.{0}.html'.format(lang['short']))
        with codecs.open(outputfile, "w", "utf-8") as f:
            f.write(template.generate(**data).render('xhtml'))
        SetFilePermissions(outputfile)
    except:
        pass

    data['dependencies'] = {}
    data['projects']     = {}

    data['headline']     = {}
    data['pdolinkname']  = {}
    data['maintainer']   = {}

    # I18n for headlines, link description, maintainer
    for status in use_dependencystatus:
        data['headline'][status]    = Markup(pkgstatus[status]['headline'])
        data['pdolinkname'][status] = Markup(pkgstatus[status]['pdolinkname'])
        if pkgstatus[status]['order'] <= pkgstatus['experimental']['order']:
            data['maintainer'][status] = _('Maintainer')
        else:
            data['maintainer'][status] = _('Responsible')

    for task in data['taskskeys']:
        data['task']               = task
        # Keep the Dependency lists per task to be able to loop over
        # all tasks in plain package list
        data['dependencies'][task] = []
        for status in use_dependencystatus:
            if len(tasks.tasks[task].dependencies[status]) > 0:
                if status != 'unknown':
                    data['dependencies'][task].append(status)
                elif lang['short'] == 'en':
                    # Just print an error message if there are packages with
                    # unknown status but only once and not per language
                    for dep in tasks.tasks[task].dependencies[status]:
                        stderr.write("Warning: Dependency with unknown status: %s\n"
                                     % dep.properties['name'])
        # Keep the Project lists per task to be able to loop over all tasks in plain package list
        data['projects'][task] = tasks.tasks[task].dependencies
        data['othertasks']     = _("Links to other tasks")
        data['indexlink']      = _("Index of all tasks")

        if data['projectname'] in ('Debian Astro', 'Debian Hamradio'):
            template = loader.load('packages.xhtml')
        else:
            template = loader.load('tasks.xhtml')

        outputfile = os.path.join(
            outputdir, '{0}.{1}.html'.format(task, lang['short']))
        with codecs.open(outputfile, "w", "utf-8") as f:
            f.write(template.generate(**data).render('xhtml'))
        SetFilePermissions(outputfile)

    template = loader.load('packagelist.xhtml')

    data['projectsintasks'] = []
    for task in data['taskskeys']:
        data['projectsintasks']     = tasks.tasks[task].dependencies

    outputfile = os.path.join(outputdir, 'packagelist.{0}.html'.format(lang['short']))
    with codecs.open(outputfile, 'w', 'utf-8') as f:
        f.write(template.generate(**data).render('xhtml'))
    SetFilePermissions(outputfile)


htafp.write("LanguagePriority {0}\n".format(
    ' '.join(lang['htaccess'] for lang in languages)))
htafp.close()
SetFilePermissions(htaccess)

UnlockBlendsTools()
